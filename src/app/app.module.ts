import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RouterModule} from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import { CustomerComponent } from './customer/customer.component';
import { FormsModule} from '@angular/forms';
import { UpdateCustomerComponent } from './update-customer/update-customer.component';
import { NewCarsComponent } from './new-cars/new-cars.component';
import { AddNewCarComponent } from './add-new-car/add-new-car.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule, MatButtonModule, MatSelectModule, MatIconModule, MatCardModule, MatDatepickerModule} from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CustomerComponent,
    UpdateCustomerComponent,
    NewCarsComponent,
    AddNewCarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'customer', component: CustomerComponent},
      {path: 'updateCustomer/:id', component: UpdateCustomerComponent},
      {path: 'newCars', component: NewCarsComponent},
      {path: 'addNewCar', component: AddNewCarComponent},
    ]),
    BrowserAnimationsModule,
    MatCardModule,
    MatDatepickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
