import { Component, OnInit } from '@angular/core';

import { CarApiService} from '../car-api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  products;

  constructor(private apiService: CarApiService) {
  }

  ngOnInit() {
    this.getAllCars();
  }

  getAllCars(): void {
    this.apiService.getAllCars()
      .subscribe((data) => {
        console.log('hello' , data);
          this.products = data;
        });
  }
  deleteCustomer = function(id) {
    if (confirm('Are you sure?')) {
      this.apiService.deleteCustomer(id)
        .subscribe(data => {
          console.log('jjjj');
          this.getAllCars();
        });
    }
  };
  downloadCustomer = function() {
    if (confirm('Are you sure you want to download the customer excel file?')) {
      this.apiService.downlaodCustomer()
        .subscribe(data => {
        });
    }
  };

  generateIndividualPdf = function(id) {
    if (confirm('Are you sure you want to download the customer excel file?')) {
      this.apiService.generateIndividualPdf(id)
        .subscribe(data => {
        });
    }
  };
}
