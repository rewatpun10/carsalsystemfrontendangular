import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewCarServiceService {

  baseurl = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }
  public getAllNewCars() {
    return this.httpClient.get(this.baseurl + '/newCars');
  }
  public postNewCars(newCar: Object) {
    console.log('post bb', newCar);
    return this.httpClient.post(this.baseurl + '/newCars', newCar);
  }
}
