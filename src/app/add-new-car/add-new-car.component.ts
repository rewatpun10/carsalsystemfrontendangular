import { Component, OnInit } from '@angular/core';
import {NewCar} from '../newCar';
import {NewCarServiceService} from '../new-car-service.service';

@Component({
  selector: 'app-add-new-car',
  templateUrl: './add-new-car.component.html',
  styleUrls: ['./add-new-car.component.css']
})
export class AddNewCarComponent implements OnInit {

  newCarObj = NewCar;
  constructor(private newCarServiceApi: NewCarServiceService) {
  }

  isAdded = false;
  ngOnInit() {
  }

  addNewCar = function(newCar) {
    this.newCarObj = {
      referenceNumber: newCar.referenceNumber,
      make: newCar.make,
      model: newCar.model,
      driveType: newCar.driveType,
      colour: newCar.colour,
      transmission: newCar.transmission,
      carEngine: newCar.carEngine,
      fuelType: newCar.fuelType,
      numberOfDoors: newCar.numberOfDoors,
      numberOfSeats: newCar.numberOfSeats,
      price: newCar.price,
      numberOfCars: newCar.numberOfCars,
      roadSideAssistancePackages: newCar.roadSideAssistancePackages,
      warranty: newCar.warranty,
      extendingWarranty: newCar.extendingWarranty
    }
    console.log('ppp', this.newCarObj);
    this.newCarServiceApi.postNewCars(this.newCarObj)
      .subscribe((data) => {
        this.customer = data;
        this.isAdded = true;
      });
  };

}
