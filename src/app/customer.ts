export class Customer {
  id: string;
  name: string;
  address: string;
  phoneNumber: string;
  emailAddress: string;
}
