import { Component, OnInit } from '@angular/core';
import { CarApiService} from '../car-api.service';
import {Customer} from '../customer';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  constructor(private customerApi: CarApiService) { }
  isAdded = false;
  id: number;
  confirmationString = 'New product has been added';
  customerObj: Customer;

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST'
    })
  };
  addNewProduct = function(customer) {
    this.customerObj = {
      name: customer.name,
      address: customer.country,
      phoneNumber: customer.phoneNumber,
      emailAddress: customer.emailAddress
    }
    this.customerApi.postCustomers(this.customerObj)
      .subscribe((data) => {
        this.customer = data;
        this.isAdded = true;
  });
  };


  ngOnInit() {
  }
}
