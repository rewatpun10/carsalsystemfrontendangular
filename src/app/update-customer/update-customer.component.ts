import { Component, OnInit } from '@angular/core';
import { CarApiService} from '../car-api.service';
import { ActivatedRoute} from '@angular/router';
import { Router} from '@angular/router';
import {Customer} from '../customer';

@Component({
  selector: 'app-update-customer',
  templateUrl: './update-customer.component.html',
  styleUrls: ['./update-customer.component.css']
})
export class UpdateCustomerComponent implements OnInit {
  id: string;
  customer;
  isUpdated = false;
  updatedString = 'has been updated successfully';
  customerObj: Customer;
  customersObj: object = {};
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private customerService: CarApiService) { }
  updateCustomer = function(customer) {
    this.customerObj = {
      id: customer.id,
      name: customer.name,
      address: customer.country,
      phoneNumber: customer.phoneNumber,
      emailAddress: customer.emailAddress
    };
    console.log('bbbb', this.customerObj);
    this.customerService.updateCustomer(this.id, this.customerObj)
      .subscribe((data) => {
        this.customer = data;
        this.isUpdated = true;
      });
  }

  ngOnInit() {
    this.customer = new  Customer();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.customerService.getCustomerById(this.id)
      .subscribe(data => {
        console.log('apple' , data);
        this.customer = data;
      });
  }

}
