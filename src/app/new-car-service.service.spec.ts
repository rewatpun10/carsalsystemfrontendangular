import { TestBed } from '@angular/core/testing';

import { NewCarServiceService } from './new-car-service.service';

describe('NewCarServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewCarServiceService = TestBed.get(NewCarServiceService);
    expect(service).toBeTruthy();
  });
});
