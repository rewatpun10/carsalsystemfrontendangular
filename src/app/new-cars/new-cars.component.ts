import { Component, OnInit } from '@angular/core';
import {NewCarServiceService} from '../new-car-service.service';

@Component({
  selector: 'app-new-cars',
  templateUrl: './new-cars.component.html',
  styleUrls: ['./new-cars.component.css']
})
export class NewCarsComponent implements OnInit {
  newCars;
  constructor(private newCarService: NewCarServiceService) {
  }

  ngOnInit() {
    this.getAllNewCars();
  }
  getAllNewCars(): void {
    this.newCarService.getAllNewCars()
      .subscribe((data) => {
        console.log('hello', data);
        this.newCars = data;
      });
  }
}
