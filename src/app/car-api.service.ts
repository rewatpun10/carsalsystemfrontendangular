import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Customer} from './customer';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarApiService {
  baseurl = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }

  public getAllCars() {
    return this.httpClient.get(this.baseurl + '/customers');
  }
  public postCustomers(cust: Object) {
    console.log('post bb', cust);
    return this.httpClient.post(this.baseurl + '/customers', cust);
  }
  public deleteCustomer(id: number) {
    console.log('delete');
    return this.httpClient.delete(this.baseurl + '/customers/' + id);
  }

  public getCustomerById(id: string) {
    console.log('get customer by ID');
    return this.httpClient.get(this.baseurl + '/customers/' + id);
  }

  public updateCustomer(id: string, customer) {
    console.log('update customer', id);
    return this.httpClient.put(this.baseurl + '/customers/' + id, customer);
  }

  public downlaodCustomer() {
    return this.httpClient.get(this.baseurl + '/downloadCustomers');
  }

  public generateIndividualPdf(id: number) {
    console.log('Hurray');
    return this.httpClient.get(this.baseurl + '/customers/generateIndividualReport/' + id);
  }
}
