export class NewCar {
  id: string;
  referenceNumber: string;
  make: string;
  model: string;
  driveType: string;
  colour: string;
  transmission: string;
  carEngine: string;
  fuelType: string;
  numberOfDoors: string;
  numberOfSeats: string;
  price: string;
  numberOfCars: string;
  warranty: number;
  roadSideAssistancePackages: string;
  extendingWarranty: number;
}
